var auto = {} //declaración de un objeto

//atributos del objeto: 
auto.color = "negro";
auto.dueno = "jose";
auto.marca = "lamborghini";
auto.modelo = "2018";
auto.info = function () //método: una función dentro de un objeto.
{
	//document.write("auto " + auto.color + ", dueño: " + auto.dueno + " marca: " + auto.marca);
	document.write("auto " + this.color + ", dueño: " + this.dueno + " marca: " + this.marca + ", modelo: " + this.modelo);
	//document.write("auto " + this.color); // con this, no importa el objeto en que esté, va a tomar el atributo color
}

//document.write("auto " + auto.color + ", dueño: " + auto.dueno + " marca: " + auto.marca);
    
auto.info();

	

