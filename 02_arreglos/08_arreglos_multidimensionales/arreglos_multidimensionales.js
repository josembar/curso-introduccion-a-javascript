var peli1 = ["Fight Club","1999","David Fincher"];
var peli2 = ["León: The Professional","1994","Luc Besson"];
var peli3 = ["Bram Stoker's Dracula","1992","Francis Ford Coppola"];

var cinema = [peli1,peli2,peli3];

console.log("Nombre de primera peli favorita: " + cinema[0][0]);
console.log("Director de segunda peli favorita: " + cinema[1][2]);
console.log("Año de tercera peli favorita: " + cinema[2][1]);

/*
var alumno1 = ["juan","ortiz","colombia"];
var alumno2 = ["pipe","jara","peru"];
var alumno3 = ["jose","perez","chile"];

var clase = [alumno1,alumno2,alumno3];
console.log(clase);
console.log(clase[0][1]);
*/