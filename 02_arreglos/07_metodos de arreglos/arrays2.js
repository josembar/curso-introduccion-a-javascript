var semana = ["Lunes","Martes","Miércoles"]

console.log("Arreglo inicial: ")
console.log(semana)
semana.push("Jueves","Viernes") //método para agregar nuevos elementos al arreglo
//console.log("Se agregó Jueves")
//console.log(semana)
//semana.push("Viernes")
console.log("5 días de la semana: ")
console.log(semana)
semana.pop() //este método borra el último elemento del arreglo
console.log("Se borra un día: ")
console.log(semana)
semana.sort() //este método ordena los elementos de forma alfabética. También cambia el orden de agrupación
console.log("Orden alfabético: ")
console.log(semana)
semana.reverse() //este comando ordena de forma alfabética inversa. Al igual que con sort() se cambia el orden de agrupación
console.log("Orden alfabético inverso: ")
console.log(semana)