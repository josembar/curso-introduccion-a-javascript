var mensajes = ["No hay yenes para cambiar",
				"Total yenes: ",
				"Monto mayor o igual a USD 1000. Dirigase a una casa de cambio"
			   ];

function convertirYenes ()
{
	var tasaCambio = 110.14302; //valor de 1 dólar en yenes
	var montoParaCambiar = document.getElementById("dolares").value;
	//getElementById() es un método que obtiene del html el elemento con el id indicado. 
	//value obtiene el valor que está en ese elemento
	var resultado = montoParaCambiar * tasaCambio;
	//alert("Total yenes: " + resultado);

	if (montoParaCambiar <= 0) 
	{
		alert(mensajes[0]);
	}

	else if (montoParaCambiar > 0 && montoParaCambiar < 1000)
	{
		alert(mensajes[1] + resultado);	
	} 

	else alert(mensajes[2]);
}