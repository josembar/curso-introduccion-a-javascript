var prueba = "hola"; //al crear la variable de forma global, esta puede ser usada en cualquier parte del código

function mostrar ()
{
	//var prueba = "hola"; //al crear la variable dentro de la función está solo puede ser usada por la misma
	prueba += " mundo";
}

mostrar();
document.write(prueba);